import 'dart:convert';
import 'dart:io';

import 'package:hive_mvvm/constants/Constants.dart';
import 'package:hive_mvvm/models/api_models/rp_post.dart';
import 'package:http/http.dart' as http;

class ApiProvider{
   Map<String, String> headers = {
     "Accept" : "application/json"
   };

   Future<List<dynamic>> fetchPosts() async{
     String url = Constants.BASE_URL+"/posts";
     final response = await http.get(url, headers: headers);
     if(response.statusCode == 200){
       var decodedBody = json.decode(response.body);
       return decodedBody;
     }else{
       throw Exception('Failed to load posts');
     }

   }
}