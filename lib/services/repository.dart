import 'package:hive_mvvm/services/api_services.dart';
import 'package:hive_mvvm/models/api_models/api_models.dart' as api;
class Repository{
  final apiProvider = ApiProvider();
  Future<List<dynamic>> fetchPosts() async {
    return apiProvider.fetchPosts();
  }

}