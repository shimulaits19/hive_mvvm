
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hive_mvvm/models/db_models/db_models.dart';
import 'package:hive_mvvm/pages/post_list.dart';
import 'package:get/get.dart';
import 'package:hive_mvvm/viewmodels/post_viewmodel.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:provider/provider.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  final appDirectory = await path_provider.getApplicationDocumentsDirectory();
  Hive.init(appDirectory.path);
  Hive.registerAdapter(DbPostAdapter());
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'MVVM APP',
      theme: ThemeData(
        primarySwatch: Colors.yellow,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => PostViewModel()),
        ],
        child: PostListPage(),
      ),
    );
  }
}


