import 'package:flutter/material.dart';
import 'package:hive_mvvm/viewmodels/viewstate.dart';

class BaseModel extends ChangeNotifier {
  ViewState _state = ViewState.Idle;

  ViewState get state => _state;

  void setState(ViewState newState){
    _state = newState;
    notifyListeners();
  }
}
