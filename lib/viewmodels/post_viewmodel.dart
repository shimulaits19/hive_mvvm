import 'package:hive_mvvm/models/api_models/api_models.dart' as api;
import 'package:hive_mvvm/models/db_models/db_post.dart';
import 'package:hive_mvvm/services/api_services.dart';
import 'package:hive_mvvm/services/hive_services.dart';
import 'package:hive_mvvm/utils/utils.dart';
import 'package:hive_mvvm/viewmodels/base_model.dart';
import 'package:hive_mvvm/viewmodels/viewstate.dart';

class PostViewModel extends BaseModel{
  ApiProvider apiProvider = ApiProvider();
  HiveServices hiveServices = HiveServices();
  List<DbPost> posts = List<DbPost>();
  Future<void> getAllPost() async{
    setState(ViewState.Busy);
    bool internetAvalilable = await isInternetAvailable();
    if(!internetAvalilable){
      print("Fetching from HIVE");
      bool exists = await hiveServices.isExists(boxName: "posts");
      if(exists){
        posts = await hiveServices.getBoxes("posts");
        setState(ViewState.Idle);
        notifyListeners();
      }else{
        posts = List();
        setState(ViewState.Idle);
        notifyListeners();
      }
    }else{
      var result  = await apiProvider.fetchPosts();
      (result as List).map((e) {
        DbPost post = DbPost(
          title: e["title"],
          id: e["id"],
          userId: e["userId"],
          body: e["body"],
        );
        posts.add(post);
        notifyListeners();
      }).toList();
     // await hiveServices.addBoxes(posts, "posts");
      print(posts.length);
      setState(ViewState.Idle);
      notifyListeners();
    }

  }
}