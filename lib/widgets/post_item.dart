import 'package:flutter/material.dart';
import 'package:hive_mvvm/models/api_models/api_models.dart';
import 'package:hive_mvvm/models/db_models/db_models.dart';

class PostItem extends StatelessWidget {
  DbPost post;
  PostItem(this.post);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 8, right: 8),
      child: Card(
        child: Container(
          height: 100,
          child: ListTile(
            title: Text('POST ID: ${post.id}'),
            subtitle: Text("${post.body}", overflow: TextOverflow.ellipsis,),
          ),
        ),
      ),
    );
  }
}
