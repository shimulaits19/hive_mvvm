import 'package:flutter/material.dart';
import 'package:hive_mvvm/constants/Constants.dart';
import 'package:hive_mvvm/models/db_models/db_models.dart';

class PostDetailsScreen extends StatelessWidget {
  DbPost post;
  PostDetailsScreen({this.post});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white
        ),
        title: Text(
          "${post.title}",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          Constants.horizontalSizedBox(8),
          Padding(
            padding: const EdgeInsets.only(left: 8, right: 8),
            child: Text("${post.title}", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
          ),
          Constants.horizontalSizedBox(8),
          Padding(
            padding: const EdgeInsets.only(left: 8, right: 8),
            child: Text("${post.body}"),
          ),
        ],
      ),
    );
  }
}
