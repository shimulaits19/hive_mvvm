import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive_mvvm/pages/post_details.dart';
import 'package:hive_mvvm/viewmodels/post_viewmodel.dart';
import 'package:hive_mvvm/viewmodels/viewstate.dart';
import 'package:hive_mvvm/widgets/post_item.dart';
import 'package:provider/provider.dart';
class PostListPage extends StatefulWidget {
  @override
  _PostListPageState createState() => _PostListPageState();
}

class _PostListPageState extends State<PostListPage> {
  PostViewModel postViewModel;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, (){
      context.read<PostViewModel>().getAllPost();

    });
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Posts",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Consumer<PostViewModel>(
        builder: (context, postViewModel, child){
          return postViewModel.state == ViewState.Busy ? Center(child: CircularProgressIndicator(),): ListView.builder(
            itemCount: postViewModel.posts.length,
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                onTap: (){
                  Get.to(PostDetailsScreen(post: postViewModel.posts[index],));
                },
                child: PostItem(postViewModel.posts[index]),
              );
            },
          );
        },
      ),
    );
  }
}
