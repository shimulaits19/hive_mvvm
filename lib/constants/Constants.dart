import 'package:flutter/cupertino.dart';

class Constants{
  static String BASE_URL = "https://jsonplaceholder.typicode.com";

  static Widget horizontalSizedBox (double height){
    var sizedBox = SizedBox(
      height: height,
    );

    return sizedBox;
  }
}