// To parse this JSON data, do
//
//     final rpPost = rpPostFromJson(jsonString);

import 'dart:convert';

List<RpPost> rpPostFromJson(String str) => List<RpPost>.from(json.decode(str).map((x) => RpPost.fromJson(x)));

String rpPostToJson(List<RpPost> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class RpPost {
  RpPost({
    this.userId,
    this.id,
    this.title,
    this.body,
  });

  int userId;
  int id;
  String title;
  String body;

  factory RpPost.fromJson(Map<String, dynamic> json) => RpPost(
    userId: json["userId"] == null ? null : json["userId"],
    id: json["id"] == null ? null : json["id"],
    title: json["title"] == null ? null : json["title"],
    body: json["body"] == null ? null : json["body"],
  );

  Map<String, dynamic> toJson() => {
    "userId": userId == null ? null : userId,
    "id": id == null ? null : id,
    "title": title == null ? null : title,
    "body": body == null ? null : body,
  };
}
