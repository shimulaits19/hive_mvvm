import 'package:hive/hive.dart';
part 'db_post.g.dart';
@HiveType(typeId: 1)
class DbPost {
  DbPost({
    this.userId,
    this.id,
    this.title,
    this.body,
  });

  @HiveField(0)
  int userId;
  @HiveField(1)
  int id;
  @HiveField(2)
  String title;
  @HiveField(3)
  String body;

}


